import React from "react";
import Courses from "./components/course/Courses";
import CourseEdit from './components/course/CourseEdit';
import { Navbar } from "./components/Navbar";
import Auth from "./components/Auth";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import "./App.css";

const Login = () => {
  return <Route path="/auth" component={Auth} />;
};

const DefaultRoutes = () => {
  return (
    <>
      <Navbar />
      <div className="container-fluid">
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/auth" />} />
          <Route exact path="/courses" component={Courses} />
          <Route exact path="/courses/:id" component={CourseEdit} />
          <Route render={() => <h3>Not Found</h3>} />
        </Switch>
      </div>
    </>
  );
};

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/auth" component={Login} />
        <Route component={DefaultRoutes} />
      </Switch>
    </Router>
  );
}

export default App;
