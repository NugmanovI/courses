import React from "react";
import { Button } from "../Button";
import { Link } from "react-router-dom";

export const CourseItem = (props) => {
  let { title, _id, description, isCompleted } = props.course;
  return (
    <div className="card m-2" style={{ width: "18rem" }}>
      <img
        src="https://petapixel.com/assets/uploads/2019/02/mooncompositemain-800x800.jpg"
        className="card-img-top"
        alt="pic"
      />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <Button href="#" isCompleted={isCompleted}>
          Go somewhere
        </Button>
        <Link className="nav-link" to={`/courses/${_id}`}>Edit</Link>
        {/* <button className="btn btn-sm btn-warning ml-2" onClick={props.openEditor}>Edit</button> */}
      </div>
    </div>
  );
};
