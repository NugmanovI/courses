import React, { Component } from "react";
import axios from "axios";

class CourseEdit extends Component {
  state = {
    title: "",
    author: "",
    description: "",
    price: ""
  };

  courseId = this.props.match.params.id

  createNew = this.courseId === 'new' ? true : false;
  // courseId = this.props.currentId

  componentDidMount() {
    if (!this.createNew) {
      axios.get(`/courses/${this.courseId}`).then(result => {
        this.setState({
          ...result.data
        });
      });
    }
  }

  handleChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  save = e => {
    e.preventDefault();
    let { title, author, description, price } = this.state;

    if (this.createNew) {
      axios.post('/courses', {
        title, author, description, price
      }).then(result => {
        console.log(result.data)
      });
    } else {
      axios.put(`/courses/${this.courseId}`, {
        title, author, description, price
      }).then(result => {
        console.log(result.data)
      });
    }
  };

  cancel = e => {
    e.preventDefault();
    // undo and redirect
  };

  delete = e => {
    e.preventDefault();
    axios.delete(`/courses/${this.courseId}`)
      .then(result => {
        console.log(result.data)
      });
  };

  render() {
    return (
      <>
        <div className="row">
          <div className="col-md-12 mt-2">
            <h3 className="float-left">{this.createNew ? 'Create New' : 'Edit'} Course</h3>
          </div>
        </div>
        <hr className="my-1" />
        <div className="row">
          <div className="col-md-6">
            <form className="card mt-2">
              <div className="card-header">
                <h4 className="m-0">{this.state.title} Course</h4>
              </div>
              <div className="card-body">
                <div className="form-row">
                  <div className="form-group col-md-6">
                    <label htmlFor="title">Title</label>
                    <input
                      name="title"
                      value={this.state.title}
                      onChange={this.handleChange}
                      type="text"
                      className="form-control"
                      id="title"
                      placeholder="Title"
                    />
                  </div>
                  <div className="form-group col-md-6">
                    <label htmlFor="author">Author</label>
                    <input
                      name="author"
                      value={this.state.author}
                      onChange={this.handleChange}
                      type="text"
                      className="form-control"
                      id="author"
                      placeholder="Author"
                    />
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="description">Description</label>
                  <textarea
                    name="description"
                    value={this.state.description}
                    onChange={this.handleChange}
                    className="form-control"
                    id="description"
                    placeholder="Description"
                  />
                </div>
                <div className="form-row">
                  <div className="form-group col-md-2">
                    <label htmlFor="price">Price</label>
                    <input
                      name="price"
                      value={this.state.price}
                      onChange={this.handleChange}
                      type="number"
                      className="form-control"
                      id="price"
                    />
                  </div>
                </div>
                <hr />

                <button className="btn btn-danger float-left" disabled={this.courseId} onClick={this.delete}>Delete</button>

                <button className="btn btn-primary ml-2 float-right" onClick={this.save}>
                  Save
            </button>
                <button className="btn btn-secondary float-right" onClick={this.cancel}>Cancel</button>
              </div>
            </form>
          </div>
        </div>
      </>
    );
  }
}

export default CourseEdit;
