import React, { Component } from "react";
import { CourseItem } from "./CourseItem";
import axios from "axios";
import { Link } from "react-router-dom";
// import CourseEdit from "./CourseEdit";

class Courses extends Component {
  state = {
    courses: [],
    editCourseId: ''
  };

  componentDidMount() {
    axios.get("/courses").then(result => {
      this.setState({
        courses: result.data
      });
    });
  }

  openEditor(index) {
    console.log('hi')
    // if (index === 'new') {
    //   this.setState({ editCourseId: index})
    // } else {
    //   const course = this.state.courses[index]
    //   this.setState({ editCourseId: course._id})
    // }
  };

  render() {
    return (
      <>
        <div className="row">
          <div className="col-md-12 mt-2">
            <h3 className="float-left">Courses</h3>
            <Link className="btn btn-sm btn-primary float-right" to='/courses/new'>Add Course</Link>
          </div>
        </div>
        <hr className="my-1" />
        <div className="row">
          <div className="col-md-6">
            {this.state.courses.map((item, index) => (
              <CourseItem key={item._id} course={item} openEditor={() => this.openEditor(index)} />
            ))}
          </div>
          {/* <div className="col-md-6">
            <CourseEdit currentId={this.state.editCourseId}/>
          </div> */}
        </div>
      </>
    );
  }
}

export default Courses;
