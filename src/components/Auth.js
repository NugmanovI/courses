import React, { Component } from 'react'
import axios from "axios";

export default class Auth extends Component {
    state = {
        userEmail: '',
        userPassword: ''
    };

    handleChange = e => {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    login = e => {
        e.preventDefault();
        axios.post("/auth/login", {
            // email: this.state.userEmail,
            // password: this.state.userPassword
            })
            .then(result => {
                console.log(result)
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <form className="form-signin">
                <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                <label htmlFor="inputEmail" className="sr-only">Email address</label>
                <input name="userEmail" value={this.state.userEmail} onChange={this.handleChange} type="email" id="userEmail" className="form-control" placeholder="Email address" required="" autoFocus="" />
                <label htmlFor="inputPassword" className="sr-only">Password</label>
                <input name="userPassword" value={this.state.userPassword} onChange={this.handleChange} type="password" id="userPassword" className="form-control" placeholder="Password" required="" />
                <div className="checkbox mb-3">
                    <label>
                        <input type="checkbox" value="remember-me" /> Remember me
                    </label>
                </div>
                <button className="btn btn-lg btn-primary btn-block" onClick={this.login} type="submit">Sign in</button>
                <p className="mt-5 mb-3 text-muted">© 2017-2019</p>
            </form>
        )
    }
}
