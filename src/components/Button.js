import styled from 'styled-components'

export const Button = styled.a`
         background: ${({ isCompleted }) =>
           isCompleted ? "green" : "red"};
         color: #fff;
         padding: 5px 10px;
         border-radius: 10px;
         transition: all ease 0.3s;
         &:hover {
           background: #fff;
           border: 1px solid black;
           color: black;
           text-decoration: none;
         }
       `;